<?php

namespace Cli\CliBundle\Command;

use Application\Task\Query;
use Domain\Task;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TaskListCommand extends ContainerAwareCommand
{
    /**
     * TaskQuery
     *
     * @var Query
     */
    protected $taskQuery;

    /**
     * TaskListCommand constructor
     *
     * @param Query $taskQuery
     *
     */
    public function __construct(Query $taskQuery)
    {
        $this->taskQuery = $taskQuery;

        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setName('task:list')
            ->setDescription('...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Remaining');

        $remainingTasks = $this->taskQuery->getAllRemainingTasks();

        /** @var Task $task */
        foreach ($remainingTasks as $task) {
            $output->writeln(' - ' . $task->getName());
        }

        $output->writeln('');

        $output->writeln('Completed');

        $completedTasks = $this->taskQuery->getAllCompletedTasks();

        /** @var Task $task */
        foreach ($completedTasks as $task) {
            $output->writeln(' - ' . $task->getName());
        }
    }

}
