<?php
/**
 * File: TaskCannotBeSavedException.php - todo
 * zzz - 03/02/17 16:00
 * PHP Version 7
 *
 * @category None
 * @package  Todo
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 */

namespace Application\Task\Exception;

/**
 * Class TaskCannotBeSavedException
 *
 * @category None
 * @package  Application\Task\Exception
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 */
class TaskCannotBeSavedException extends \Exception
{

}