<?php
/**
 * File: TaskCannotBeRemovedException.php - todo
 * zzz - 03/02/17 16:51
 * PHP Version 7
 *
 * @category None
 * @package  Todo
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 */

namespace Application\Task\Exception;

/**
 * Class TaskCannotBeRemovedException
 *
 * @category None
 * @package  Application\Task\Exception
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 */
class TaskCannotBeRemovedException extends \Exception
{

}