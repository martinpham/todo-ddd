<?php
/**
 * File: TaskNotFoundException.php - todo
 * zzz - 03/02/17 13:37
 * PHP Version 7
 *
 * @category None
 * @package  Todo
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 */

namespace Domain\Exception;

/**
 * Class TaskNotFoundException
 *
 * @category None
 * @package  Domain\Exception
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 */
class TaskNotFoundException extends \Exception
{

}