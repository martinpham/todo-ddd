<?php
/**
 * File: TaskNameIsAlreadyExistedException.php - todo
 * zzz - 03/02/17 14:49
 * PHP Version 7
 *
 * @category None
 * @package  Todo
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 */

namespace Domain\Exception;

/**
 * Class TaskNameIsAlreadyExistedException
 *
 * @category None
 * @package  Domain\Exception
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 */
class TaskNameIsAlreadyExistedException extends \Exception
{

}