<?php

namespace Domain\Specification;

use Domain\Exception\TaskNotFoundException;
use Domain\Repository\TaskRepositoryInterface;

class TaskNameIsUniqueSpecification
{
    /**
     * TaskRepository
     *
     * @var TaskRepositoryInterface
     */
    protected $taskRepository;

    /**
     * TaskNameIsUniqueSpecification constructor
     *
     * @param TaskRepositoryInterface $taskRepository Task Repository
     */
    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * Is Satisfied By
     *
     * @param string $name Name
     *
     * @return bool
     */
    public function isSatisfiedBy(string $name)
    {
        try {
            $task = $this->taskRepository->findByName($name);
        } catch (TaskNotFoundException $e) {
            return true;
        }

        return false;

    }



}
