<?php

namespace Web\FrontendBundle\Controller;

use Application\Task\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class TaskController
 *
 * @category None
 * @package  Web\FrontendBundle\Controller
 * @author   Martin Pham <i@martinpham.com>
 * @license  None http://
 * @link     None
 *
 * @Route("/task")
 */
class TaskController extends Controller
{
    /**
     * @Route("/list")
     * @Template()
     */
    public function listAction(
        Query $taskQuery
    ) {
        return [
            'remaining_tasks' => $taskQuery->getAllRemainingTasks(),
            'completed_tasks' => $taskQuery->getAllCompletedTasks()
        ];
    }

    /**
     * @Route("/create")
     */
    public function createAction()
    {
        return $this->render('FrontendBundle:Task:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/updateStatus")
     */
    public function updateStatusAction()
    {
        return $this->render('FrontendBundle:Task:update_status.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/update")
     */
    public function updateAction()
    {
        return $this->render('FrontendBundle:Task:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction()
    {
        return $this->render('FrontendBundle:Task:delete.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/clean")
     */
    public function cleanAction()
    {
        return $this->render('FrontendBundle:Task:clean.html.twig', array(
            // ...
        ));
    }

}
